const path = require('path')

module.exports = function (/* ctx */) {
  return {
    boot: [
    ],

    css: [
      'app.sass'
    ],

    extras: [
      'roboto-font',
      'material-icons'
    ],

    framework: {
      iconSet: 'material-icons',
      lang: 'en-us',

      all: false,
      components: [
        'QLayout',
        'QHeader',
        'QDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QIcon',
        'QList',
        'QItem',
        'QItemSection',
        'QItemLabel'
      ],

      directives: [
        'Ripple'
      ],

      plugins: []
    },

    supportIE: false,

    supportTS: false,

    build: {
      scopeHoisting: true,
      vueRouterMode: 'history',
      showProgress: false,
      gzip: false,
      analyze: true,

      transpileDependencies: [
        /amcharts/,
        /vue-echarts/,
        /resize-detector/
      ],

      chainWebpack (chain) {
        chain.resolve.alias
          .set('utils', path.resolve(__dirname, './src/utils'))
      }
    },

    vendor: {
      remove: [ 'pdfmake', 'canvg', 'xlsx' ]
    },

    devServer: {
      https: false,
      port: 8080,
      open: true
    },

    animations: [],

    ssr: {
      pwa: false
    },

    pwa: {
      workboxPluginMode: 'GenerateSW',
      workboxOptions: {},
      manifest: {
        name: 'Quasar Selective Prefetch',

        short_name: 'Quasar Selective Prefetch',
        description: '',
        display: 'standalone',
        orientation: 'portrait',

        background_color: '#ffffff',

        theme_color: '#027be3',
        icons: [
          {
            src: 'statics/icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    }
  }
}
