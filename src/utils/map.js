import _ from 'lodash'

import {
  create as AM4Create,
  useTheme as AM4UseTheme,
  color as AM4Color
} from '@amcharts/amcharts4/core'
import {
  MapChart as AM4MapChart,
  MapPolygonSeries as AM4MapPolygonSeries,
  GraticuleSeries as AM4GraticuleSeries,
  projections as AM4Projections
} from '@amcharts/amcharts4/maps'
import AM4ThemeAnimated from '@amcharts/amcharts4/themes/animated'

// set am4 general theme
AM4UseTheme(AM4ThemeAnimated)

// constants
const REGIONS = {
  World: {
    homeZoomLevel: 1,
    homeGeoPoint: { longitude: 0, latitude: 11 }
  },
  Asia: {
    countries: [
      'AF', 'AM', 'AZ', 'BH', 'BD', 'BT', 'BN', 'KH', 'CN', 'GE', 'HK', 'IN',
      'ID', 'IR', 'IQ', 'IL', 'JP', 'JO', 'KZ', 'KW', 'KG', 'LA', 'LB', 'MO',
      'MY', 'MV', 'MN', 'MM', 'NP', 'KP', 'OM', 'PK', 'PH', 'QA', 'RU', 'SA',
      'SG', 'KR', 'LK', 'SY', 'TL', 'TW', 'TJ', 'TH', 'TR', 'TM', 'AE', 'UZ',
      'VN', 'YE'
    ],
    homeZoomLevel: 2.5,
    homeGeoPoint: { longitude: 87, latitude: 26 }
  },
  Europe: {
    countries: [
      'AL', 'AD', 'AT', 'BY', 'BE', 'BA', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE',
      'FO', 'FI', 'FR', 'DE', 'GI', 'GR', 'HU', 'IS', 'IE', 'IM', 'IT', 'XK',
      'LV', 'LI', 'LT', 'LU', 'MK', 'MT', 'MD', 'MC', 'ME', 'NL', 'NO', 'PL',
      'PT', 'RO', 'SM', 'RS', 'SK', 'SI', 'ES', 'SE', 'CH', 'UA', 'GB', 'VA'
    ],
    homeZoomLevel: 3.5,
    homeGeoPoint: { longitude: 11, latitude: 54 }
  },
  Africa: {
    countries: [
      'DZ', 'AO', 'BJ', 'BW', 'BF', 'BI', 'CM', 'CV', 'CF', 'KM', 'CD', 'DJ',
      'EG', 'GQ', 'ER', 'ET', 'GA', 'GM', 'GH', 'GN', 'GW', 'CI', 'KE', 'LS',
      'LR', 'LY', 'MG', 'MW', 'ML', 'MR', 'MU', 'MA', 'MZ', 'NA', 'NE', 'NG',
      'CG', 'RE', 'RW', 'SH', 'ST', 'SN', 'SC', 'SL', 'SO', 'ZA', 'SS', 'SD',
      'SZ', 'TD', 'TZ', 'TG', 'TN', 'UG', 'EH', 'ZM', 'ZW'
    ],
    homeZoomLevel: 2.75,
    homeGeoPoint: { longitude: 15, latitude: 2 }
  },
  'South America': {
    countries: [
      'AR', 'AW', 'BO', 'BR', 'CL', 'CO', 'EC', 'FK', 'GY', 'PY', 'PE', 'SR',
      'UY', 'VE'
    ],
    homeZoomLevel: 2.75,
    homeGeoPoint: { longitude: -68, latitude: -28 }
  },
  'North America': {
    countries: [
      'AI', 'AG', 'BB', 'BL', 'BM', 'BS', 'BZ', 'CA', 'CR', 'CU', 'CW', 'DM',
      'DO', 'GD', 'GP', 'GT', 'HT', 'HN', 'JM', 'KN', 'KY', 'LC', 'MF', 'MS',
      'MX', 'NI', 'PA', 'PR', 'PM', 'SM', 'SV', 'TT', 'US', 'VC', 'VG'
    ],
    homeZoomLevel: 2.25,
    homeGeoPoint: { longitude: -106, latitude: 45 }
  },
  Oceania: {
    countries: [
      'AS', 'AU', 'CK', 'FJ', 'PF', 'GU', 'KI', 'MH', 'FM', 'NR', 'NC', 'NZ',
      'NU', 'NF', 'MP', 'PW', 'PG', 'PN', 'WS', 'SB', 'TK', 'TV', 'VU'
    ],
    homeZoomLevel: 3.5,
    homeGeoPoint: { longitude: 137, latitude: -25 }
  }
}

class AM4Map {
  constructor (htmlElement, data = [], options = {}) {
    this.map = AM4Create(htmlElement, AM4MapChart)

    this.data = data
    this.options = options

    // init
    this.series = {}
    this._constructMap()

    // set geodata if available
    this.map.geodata = options.geodata

    if (!options.enablePan) {
      // disable pan
      this.map.seriesContainer.draggable = false
      this.map.seriesContainer.resizable = false
    }

    if (!options.enableZoom) {
      // disable zoom
      this.map.chartContainer.wheelable = false
      this.map.seriesContainer.events.disableType('doublehit')
      this.map.chartContainer.background.events.disableType('doublehit')
    }
  }

  dispose () {
    this._validateState()
    this.map.dispose()
    this.map = null
  }

  // private methods
  _validateState () {
    if (this.map === null) {
      throw new Error('Map already disposed')
    }
  }

  _constructMap () {
    // create a new polygon series for each region
    let polygonSeries = this.map.series.push(new AM4MapPolygonSeries())
    polygonSeries.useGeodata = true

    // set custom data if available
    polygonSeries.data = this.data

    // set defaults
    this._setDefaultPolygonTemplateProperties(polygonSeries)
    this._setTooltip(polygonSeries)
    this._setHoverState(polygonSeries)
  }

  _setDefaultPolygonTemplateProperties (series) {
    let polygonTemplate = series.mapPolygons.template
    polygonTemplate.nonScalingStroke = true
    polygonTemplate.fill = AM4Color('#EDE7F6')
    polygonTemplate.stroke = AM4Color('#000000')
    polygonTemplate.strokeOpacity = 0.2
    polygonTemplate.tooltipText = '{name} ({id})'

    // allow selected overrides via custom data
    polygonTemplate.propertyFields.fill = 'fill'
  }

  _setTooltip (series) {
    let tooltip = series.tooltip
    tooltip.getFillFromObject = false
    tooltip.background.fill = AM4Color('#000000')
    tooltip.background.fillOpacity = 0.5
    tooltip.background.strokeWidth = 0
    tooltip.background.cornerRadius = 4
    tooltip.background.pointerLength = 0
    tooltip.dx = 10
    tooltip.dy = 60
  }

  _setHoverState (series) {
    let hover = series.mapPolygons.template.states.create('hover')
    hover.properties.fitPointerToBounds = true
    hover.properties.fill = AM4Color('#FFDF33')
    hover.properties.stroke = AM4Color('#000000')
    hover.properties.strokeOpacity = 1
  }
}

export class AM4WorldMap extends AM4Map {
  // public methods
  getRegions () {
    this._validateState()
    return Object.keys(REGIONS)
  }

  focusRegion (regionName = 'World') {
    this._validateState()
    let region = REGIONS[regionName]

    // hide all series
    _(this.series).each(series => series.hide())

    // set a new projection - to force repainting of the map
    this.map.projection = regionName === 'World'
      ? new AM4Projections.NaturalEarth1()
      : new AM4Projections.Mercator()

    // set focus
    this.map.homeZoomLevel = region.homeZoomLevel
    this.map.homeGeoPoint = region.homeGeoPoint

    // show this series
    this.series[regionName].show()

    // show grid only on the World Map
    this.series.grid[regionName === 'World' ? 'show' : 'hide']()
  }

  // private method overrides
  _constructMap () {
    _.each(REGIONS, (region, name) => {
      // create a new polygon series for each region
      let polygonSeries = this.map.series.push(new AM4MapPolygonSeries())
      polygonSeries.useGeodata = true
      polygonSeries.include = region.countries

      // set custom data if available
      polygonSeries.data = _.get(this.data, name, [])

      // remove Antarctica
      polygonSeries.exclude = [ 'AQ' ]

      // hide series initially
      polygonSeries.hidden = true

      // set defaults
      this._setDefaultPolygonTemplateProperties(polygonSeries)
      this._setTooltip(polygonSeries)
      this._setHoverState(polygonSeries)

      // add series layer to map
      // and save reference to series for future manipulation
      this.series[name] = polygonSeries
    })

    // add grid
    this.series.grid = this.map.series.push(new AM4GraticuleSeries())
  }
}

export class AM4CountryMap extends AM4Map { }
